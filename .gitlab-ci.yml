workflow:
  rules:
    - if: $CI_COMMIT_BRANCH != "init" && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: never
    - when: always

variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE
  DEV_SERVER_HOST: 65.0.94.74 
  DEV_ENDPOINT: http://ec2-65-0-94-74.ap-south-1.compute.amazonaws.com:3000
  STAGING_SERVER_HOST: 65.0.94.74 
  STAGING_ENDPOINT: http://ec2-65-0-94-74.ap-south-1.compute.amazonaws.com:4000
  PRODUCTION_SERVER_HOST: 65.0.94.74 
  PRODUCTION_ENDPOINT: http://ec2-65-0-94-74.ap-south-1.compute.amazonaws.com:5000

stages:
  - test
  - build
  - deploy_dev
  - deploy_staging
  - deploy_production

run_unit_tests:
  image: node:17-alpine3.14
  stage: test
  cache:
    key: "$CI_COMMIT_REF_NAME"
    paths:
      - app/node_modules
  tags:  
    - ec2
    - docker
    - remote
  before_script:
    - cd app
    - npm install
  script:
    - npm test
  artifacts:
    when: always
    paths:
      - "app/junit.xml"
    reports:
      junit: app/junit.xml

sast:
  stage: test

build_image:
  stage: build
  tags:
    - ec2
    - shell
    - cloud
  before_script:
    - export PKG_JSON_VERSION=$(cat app/package.json | jq -r .version)
    - export VERSION=$PKG_JSON_VERSION.$CI_PIPELINE_IID
    - echo "VERSION=$VERSION" >> build.env
  script:
    - docker build -t $IMAGE_NAME:$VERSION .
  artifacts:
    reports:
      dotenv: build.env

push_image:
  stage: build
  needs:
    - build_image
  dependencies:
    - build_image
  tags:
    - ec2
    - shell
    - cloud
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
  script:
    - docker push $IMAGE_NAME:$VERSION

deploy_to_dev:
  stage: deploy_dev
  tags:
    - ec2
    - shell
    - cloud
  before_script:
    - chmod 400 $AWS_DEPLOYMENT_PEM
  script:
    - scp -o StrictHostKeyChecking=no -i $AWS_DEPLOYMENT_PEM ./docker-compose.yaml ubuntu@$DEV_SERVER_HOST:/home/ubuntu
    - ssh -o StrictHostKeyChecking=no -i $AWS_DEPLOYMENT_PEM ubuntu@$DEV_SERVER_HOST "
        docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com &&

        export COMPOSE_PROJECT_NAME=dev &&
        export DC_IMAGE_NAME=$IMAGE_NAME &&
        export DC_IMAGE_TAG=$VERSION &&
        export DC_APP_PORT=3000

        docker compose down &&
        docker compose up -d"
  environment:
    name: development
    url: $DEV_ENDPOINT

run_functional_tests:
  stage: deploy_dev
  needs: 
    - deploy_to_dev
  script:
    - echo "Running functional tests"

deploy_to_staging:
  stage: deploy_staging
  tags:
    - ec2
    - shell
    - cloud
  before_script:
    - chmod 400 $AWS_DEPLOYMENT_PEM
  script:
    - scp -o StrictHostKeyChecking=no -i $AWS_DEPLOYMENT_PEM ./docker-compose.yaml ubuntu@$STAGING_SERVER_HOST:/home/ubuntu
    - ssh -o StrictHostKeyChecking=no -i $AWS_DEPLOYMENT_PEM ubuntu@$STAGING_SERVER_HOST "
        docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com &&

        export COMPOSE_PROJECT_NAME=staging &&
        export DC_IMAGE_NAME=$IMAGE_NAME &&
        export DC_IMAGE_TAG=$VERSION &&
        export DC_APP_PORT=4000

        docker compose down &&
        docker compose up -d"
  environment:
    name: staging
    url: $STAGING_ENDPOINT

run_performance_tests:
  stage: deploy_staging
  needs: 
    - deploy_to_staging
  script:
    - echo "Running performance tests"

deploy_to_production:
  stage: deploy_production
  tags:
    - ec2
    - shell
    - cloud
  before_script:
    - chmod 400 $AWS_DEPLOYMENT_PEM
  script:
    - scp -o StrictHostKeyChecking=no -i $AWS_DEPLOYMENT_PEM ./docker-compose.yaml ubuntu@$PRODUCTION_SERVER_HOST:/home/ubuntu
    - ssh -o StrictHostKeyChecking=no -i $AWS_DEPLOYMENT_PEM ubuntu@$PRODUCTION_SERVER_HOST "
        docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com &&

        export COMPOSE_PROJECT_NAME=production &&
        export DC_IMAGE_NAME=$IMAGE_NAME &&
        export DC_IMAGE_TAG=$VERSION &&
        export DC_APP_PORT=5000

        docker compose down &&
        docker compose up -d"
  environment:
    name: production
    url: $PRODUCTION_ENDPOINT
  when: manual

include:
  - template: Jobs/SAST.gitlab-ci.yml